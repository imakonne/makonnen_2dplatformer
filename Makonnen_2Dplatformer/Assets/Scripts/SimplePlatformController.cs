﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class SimplePlatformController : MonoBehaviour { 

    [HideInInspector] public bool facingRight = true;
    [HideInInspector] public bool jump = false;
    [HideInInspector] public int timesJumped = 0;

    public float timeLeft;
    public float moveForce = 365f;
    public float maxSpeed = 5f;
    public float jumpForce = 1000f;
    public Transform groundCheck;
    private bool grounded = false;
    private Animator anim;
    private Rigidbody2D rb2d;
    public Text TimerText;
    public int counter;
    public int TotalCount;
    public Text platCount;

    void Awake()
    {
        anim = GetComponent<Animator>();
        rb2d = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        platCount.text = "Platforms tagged: " + counter.ToString() + "/" + TotalCount.ToString();
        if (counter >= TotalCount)
        {
            print("you got em all!!");
        }
 
        timeLeft -= Time.deltaTime;
        int timeRounded = (int)(timeLeft);
        TimerText.text = "Time Left: " + timeRounded.ToString();

        if (timeLeft <= 0)
        {
            Application.LoadLevel(Application.loadedLevel);
        }

        grounded = Physics2D.Linecast(transform.position, groundCheck.position, 1 << LayerMask.NameToLayer("Ground"));

        if (grounded)
        {
            timesJumped = 0;
        }

        if (Input.GetButtonDown("Jump"))
        {
            timesJumped += 1;
            if (timesJumped < 2)
                jump = true;
            
        }
    }

    void FixedUpdate()
    {
        float h = Input.GetAxis("Horizontal");

        anim.SetFloat("Speed", Mathf.Abs(h));

        if (h * rb2d.velocity.x < maxSpeed)
            rb2d.AddForce(Vector2.right * h * moveForce);

        if (Mathf.Abs(rb2d.velocity.x) > maxSpeed)
            rb2d.velocity = new Vector2(Mathf.Sign(rb2d.velocity.x) * maxSpeed, rb2d.velocity.y);

        if (h > 0 && !facingRight)
            Flip();
        else if (h < 0 && facingRight)
            Flip();

        if (jump)
        {
            anim.SetTrigger("Jump");
            rb2d.AddForce(new Vector2(50f, jumpForce));
            jump = false;
        }
    }


    void Flip()
    {
        facingRight = !facingRight;
        Vector3 theScale = transform.localScale;
        theScale.x *= -1;
        transform.localScale = theScale;




    }
}
