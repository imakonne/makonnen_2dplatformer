﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Change_Color : MonoBehaviour
{
  
  public Color red;
  public Color purple;
   public Color green;

    SpriteRenderer sprite;

    bool tagged = false;

    private void Start()
    {
        sprite = GetComponent<SpriteRenderer>();

    }

    //Start is called before the first frame update
    void OnCollisionEnter2D(Collision2D other) {

        if (other.gameObject.CompareTag("Player"))
         {
            //transform.GetComponent<Renderer>().material.color = red;

            if (Random.value < .33f)
                sprite.color = red;
            else
                if (Random.value < .5f)
                sprite.color = purple;
            else
                sprite.color = green;
            if (tagged == false)
            {
                tagged = true;
                SimplePlatformController spc = other.gameObject.GetComponent<SimplePlatformController>();
                spc.counter += 1;
            }

        }


    }

    // Update is called once per frame
    void Update()
    {
        
    }
    
}
